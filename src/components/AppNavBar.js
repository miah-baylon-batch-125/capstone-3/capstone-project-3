import React, {useContext, Fragment} from 'react';
import {Link, NavLink, useHistory} from 'react-router-dom';
import {Nav, Navbar} from 'react-bootstrap';
import UserContext from './../UserContext';

export default function AppNavbar(){

  const {user, unsetUser} = useContext(UserContext);

  let history = useHistory();

  const logout = () => {
    unsetUser();
    history.push('/');
  }

  let userLogIn = (user.id !== null) ?
      (user.isAdmin === true) ?
        <Fragment>
          <Nav.Link as={NavLink} to="/addbooks">ADD NEW BOOK</Nav.Link>
          <Nav.Link as={NavLink} to="/archivebooks">ARCHIVE BOOK</Nav.Link>
          <Nav.Link as={NavLink} to="/unarchivebooks">UNARCHIVE BOOK</Nav.Link>
          <Nav.Link as={NavLink} to="/books">ADMIN DASHBOARD</Nav.Link>
          <Nav.Link onClick={logout}>LOGOUT</Nav.Link>
        </Fragment>
      :
         <Fragment>
          <Nav.Link as={NavLink} to="/books">ALL BOOKS</Nav.Link>
          <Nav.Link  onClick={logout}>LOGOUT</Nav.Link>
        </Fragment>
  :
    (

      <Fragment>
        <Nav.Link as={NavLink} to="/collectionofbooks">COLLECTIONS</Nav.Link>
        <Nav.Link as={NavLink} to="/register">REGISTER</Nav.Link>
        <Nav.Link as={NavLink} to="/login">LOGIN</Nav.Link>
      </Fragment>

    )


  return (
    <Navbar bg="info" expand="lg">
      <Navbar.Brand as={Link} to="/">LIBROry</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Link as={NavLink} to="/">HOME</Nav.Link>
        </Nav>
        <Nav>
          {userLogIn}
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    )
}

