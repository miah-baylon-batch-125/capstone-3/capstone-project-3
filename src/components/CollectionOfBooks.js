import React from 'react';
import {Card} from 'react-bootstrap';

export default function CollectionOfBooks({bookProp}){

	const {productName, productDescription, price} = bookProp;

	return(
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{productName}</Card.Title>
				<h5>Book Summary:</h5>
				<p>{productDescription}</p>
				<h5>Price:</h5>
				<p>{price}</p>
			</Card.Body>
		</Card>
	)
}




	

	