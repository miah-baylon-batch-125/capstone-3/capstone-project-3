import React, {useState, useEffect} from 'react';
import {Container, Table} from 'react-bootstrap';

export default function AdminView(props){
	const { bookData} = props;
	const [books, setBooks] = useState([]);

	let token = localStorage.getItem('token');


	const productDetails = (bookId) => {
		fetch(`https://polar-brushlands-73295.herokuapp.com/ecommerce/products/products/${bookId}`,{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setBooks(result);
		})
	}


	useEffect( () => {
		const arrayOfBooks = bookData.map( (books) => {
			return(
				<tr key={books._id}>
					<td>{books.productName}</td>
					<td>{books.productDescription}</td>
					<td>{books.price}</td>
					<td>
						{
							(books.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
				</tr>
			)
		})

		setBooks(arrayOfBooks)
	}, [])


	

	return(
		<Container>
			<Table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
					</tr>
				</thead>
				<tbody>
					{books}
				</tbody>
			</Table>
		</Container>
	)
}