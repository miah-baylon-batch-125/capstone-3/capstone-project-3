import React from 'react';
import './Style.css';


import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap';

export default function Home(){

	return (

	<Container fluid>
		<Row>
			<Col className="px-0">
				<Jumbotron fluid className="App-bg">
 					 <h1>Welcome reader!</h1>
					  <p>
					   Get your dream book here.
					  </p>
					   <Button className="App-btn">Browse our collections</Button>
						{/*<Nav.Link as={NavLink} to="/" >Browse our collections</Nav.Link>*/}
					</Jumbotron>
				</Col>
			</Row>
	</Container>

		)
}